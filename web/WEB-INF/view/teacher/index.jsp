<%-- 
    Document   : index
    Created on : 24-feb-2017, 18:13:29
    Author     : usuario
--%>

<%@page import="model.Teacher"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="teachers" class="java.util.ArrayList" scope="request"/>  


<!DOCTYPE html>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1>Lista de profesores </h1>
    <p>
        <a href="<%= request.getContextPath()%>/teacher/create">Nuevo profesor</a>
    </p>
    <table>
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>telefono</th>
        </tr>
        <%        Iterator<model.Teacher> iterator = teachers.iterator();
            while (iterator.hasNext()) {
                Teacher teacher = iterator.next();%>
        <tr>
            <td><%= teacher.getId()%></td>
            <td><%= teacher.getName()%></td>
            <td><%= teacher.getSurname()%></td>
            <td><%= teacher.getPhone()%></td>
            <td> 
                <a href="<%= request.getContextPath() + "/teacher/delete/" + teacher.getId()%>"> Borrar</a>
                <a href="<%= request.getContextPath() + "/teacher/edit/" + teacher.getId()%>"> Editar</a>
            </td>
        </tr>
        <%
            }
        %>          </table>

</div>
<%@include file="/WEB-INF/view/footer.jsp" %>