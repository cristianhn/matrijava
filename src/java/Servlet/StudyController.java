/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Study;
import persistence.StudyDAO;

/**
 *
 * @author usuario
 */
public class StudyController extends BaseController {

    private static final Logger LOG = Logger.getLogger(StudyController.class.getName());
    private StudyDAO studyDAO;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void index() {

        //objeto persistencia
        studyDAO = new StudyDAO();
        ArrayList<Study> studies = null;//DTO: Es donde tenemos los datos, clase study

        //leer datos de la persistencia
        synchronized (studyDAO) {
            studies = studyDAO.getAll();
        }
        request.setAttribute("studies", studies);
        String name = "index";
        LOG.info("En ModuleController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/view/study/index.jsp");
    }

    public void create() {
        dispatch("/WEB-INF/view/study/create.jsp");
    }

    public void insert() throws IOException {

        //objeto persistencia
        studyDAO = new StudyDAO();
        LOG.info("crear DAO");
        //crear objeto del formulario
        Study study = loadFromRequest();
        
        synchronized (studyDAO) {
            try {
                studyDAO.insert(study);
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
                request.setAttribute("ex", ex);
                request.setAttribute("msg", "Error de base de datos ");
                dispatch("/WEB-INF/view/error/index.jsp");
            }
        }
        redirect(contextPath + "/study/index");
    }

    public void edit(String idString) throws SQLException {
        LOG.info("idString");
        long id = toId(idString);
        StudyDAO  studyDao = new StudyDAO();
        Study study = studyDao.get(id);
        request.setAttribute("study", study);
        dispatch("/WEB-INF/view/study/edit.jsp");

    }

    public void update() throws IOException, SQLException {
        StudyDAO  studyDao = new StudyDAO();
        Study study = loadFromRequest();
        studyDao.update(study);
        response.sendRedirect(contextPath + "/study");
        return;
    }

    public void delete(String idString) throws IOException, SQLException 
    {
        LOG.info("BORRANDO " + idString);
        long id = toId(idString);
        
        StudyDAO  studyDao = new StudyDAO();
        studyDao.delete(id);
        redirect(contextPath + "/study");

    }
    
    private Study loadFromRequest()
    {
        Study study = new Study();
        LOG.info("Crear modelo");
        study.setId(toId(request.getParameter("id")));
        study.setCode(request.getParameter("code"));
        study.setName(request.getParameter("name"));
        study.setShortName(request.getParameter("shortName"));
        study.setAbreviation(request.getParameter("abreviation"));
        LOG.info("Datos cargados");
        return study;
    }
}
