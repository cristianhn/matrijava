/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import static Servlet.FrontController.PARAMETERS_ATTRIBUTE;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usuario
 */
public class ModuleController extends BaseController {

    private static final Logger LOG = Logger.getLogger(ModuleController.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   public void index()
    {
        String name = "index";
        LOG.info("En ModuleController->" + name);
        request.setAttribute("msg", name );
        dispatch("/WEB-INF/view/module/index.jsp");
    }
    
    public void create()
    {
        String name = "create";
        LOG.info("En ModuleController->" + name);
        request.setAttribute("msg", name );
        dispatch("/WEB-INF/view/module/index.jsp");
    }
    
    public void insert()
    {
        String name = "insert";
        LOG.info("En ModuleController->" + name);
        request.setAttribute("msg", name );
        dispatch("/WEB-INF/view/module/index.jsp");
    }
    
    public void edit()
    {
        String name = "edit";
        LOG.info("En ModuleController->" + name);
        request.setAttribute("msg", name );
        dispatch("/WEB-INF/view/module/index.jsp");
    }
    
    public void update()
    {
        String name = "update";
        LOG.info("En ModuleController->" + name);
        request.setAttribute("msg", name );
        dispatch("/WEB-INF/view/module/index.jsp");
    }
      
    public void delete(String id)
    {        
        String name = "delete";
        LOG.info("En ModuleController->" + name);
        request.setAttribute("msg", name );
        dispatch("/WEB-INF/view/module/index.jsp");
    }
    
}
