/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

//import java.sql.Connection;
//import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Study;

/**
 *
 * @author alumno
 */
public class StudyDAO extends BaseDAO {

    private static final Logger LOG = Logger.getLogger(StudyDAO.class.getName());


    public StudyDAO() {
//        Class.forName("com.mysql.jdbc.Driver");
//        this.connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
    }


    public ArrayList<Study> getAll() {
        PreparedStatement stmt = null;
        ArrayList<Study> studies = null;

        try {
            this.connect();
            stmt = connection.prepareStatement("select * from studies");
            ResultSet rs = stmt.executeQuery();
            studies = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Study study = new Study();
                study.setId(rs.getLong("id"));
                study.setCode(rs.getString("code"));
                study.setName(rs.getString("name"));
                study.setShortName(rs.getString("shortname"));
                study.setAbreviation(rs.getString("abreviation"));

                studies.add(study);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            Logger.getLogger(StudyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return studies;
    }

    public void insert(Study study) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "INSERT INTO studies(code, name, shortName, abreviation)"
                + " VALUES(?, ?, ?, ?)"
        );
        stmt.setString(1, study.getCode());
        stmt.setString(2, study.getName());
        stmt.setString(3, study.getShortName());
        stmt.setString(4, study.getAbreviation());

        stmt.execute();
        this.disconnect();
    }

    public void delete(long id) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "DELETE FROM studies"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        stmt.execute();
        this.disconnect();
    }

    public Study get(long id) throws SQLException {
        LOG.info("get(id)");
        Study study = new Study();
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM studies"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        ResultSet rs = stmt.executeQuery();
        LOG.info("consulta hecha");
        if (rs.next()) {
            LOG.info("Datos ...");
            study.setId(rs.getLong("id"));
            study.setCode(rs.getString("code"));
            study.setName(rs.getString("name"));
            study.setShortName(rs.getString("shortName"));
            study.setAbreviation(rs.getString("abreviation"));
            LOG.info("Datos cargados");
        } else {
            LOG.log(Level.INFO, "No hay datos para el id {0}", id);
        }
        this.disconnect();
        return study;
    }

    public void update(Study study) throws SQLException {
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "UPDATE studies SET "
                        + "code = ?, "
                        + "name = ?, "
                        + "shortName = ?, "
                        + "abreviation = ? "
                        + " WHERE id = ? "
        );
        stmt.setString(1, study.getCode());
        stmt.setString(2, study.getName());
        stmt.setString(3, study.getShortName());
        stmt.setString(4, study.getAbreviation());
        stmt.setLong(5, study.getId());
        LOG.info("id : " + study.getId());
        stmt.execute();
        this.disconnect();
        return;
    }
}
